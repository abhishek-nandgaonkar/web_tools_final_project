  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix = "form" %>
<%@ page session="true" %>
<html>
<head>
	<title>Patient Login Page</title>
	<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
<p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
<h1 align="center">
	Patient Log In Page
</h1>
<form:form method = "POST" commandName="patient" action="patientLogin">
     <table align="center">
         <tr>
              <td>Username:</td>
              <td><form:input path="username"/><br></td>
              <td><form:errors path="username"></form:errors></td>
         </tr>
         <tr>
             <td>Password</td>
             <td><form:password path="password"/><br></td>
             <td><form:errors path="password"/></td>
         </tr>
         <tr>
             <td><input type = "submit" name="submit" value="Login"/><br></td>
        
         	<!-- <td> <input type="checkbox" name="Remember" value="Remember" checked> Remember Me<br></td> -->
         	
         	<td><a href = "patientRegistration">Register for a new account</a><br/></td>
         </tr>
         
    </table>
</form:form>

</body>
</html>

