<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
  <table align="center">
            <tr>
            <td><a href="doctorSearch">Search</a></td>
            </tr>
            <tr>
            <td><a href="myPatients">My Patients</a></td>
            </tr>
            <tr>
            <td><a href="taskList" >Task List</a></td>
            </tr>
            <tr>
            <td><a href="doctorSettings" >Settings</a></td>
            </tr>
            <tr>
            <td><a href="logout" target="_parent">Log Out</a></td>
            </tr>
        </table>

</body>
</html>