  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix = "form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Employee Login Page</title>
	<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
<p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
<h1 align="center">
	Doctor Log In Page
</h1>
<form:form method = "POST" commandName="doctor" action="doctorLogin">


<ul style="color: red">
            <form:errors path="*"/>
            </ul>
     <table align="center">
         <tr>
              <td>Username:</td>
              <td><form:input path="username"/><br></td>
              <td><font color="red" size="2"><i><form:errors path="username"/></i></font></td>
         </tr>
         <tr>
             <td>Password</td>
             <td><form:password path="password"/><br></td>
             <td><font color="red" size="2"><i><form:errors path="password"/></i></font></td>
         </tr>
         <tr>
             <td><input type = "submit" name="submit" value="Login"/><br/></td>
        
         	<td><!--  <input type="checkbox" name="Remember" value="Remember" checked> Remember Me<br> -->
         	<a href = "doctorRegistration">Register for a new account</a><br/></td>
         	
         </tr>
        <!--  <tr>
             <td><a href = "doctorRegistration">Register for a new account</a><br/></td>
         </tr> -->
    </table>
</form:form>
<!-- <a href = "doctorRegistration">Register for a new account</a><br/> -->
</body>
</html>

