<%@ page language="java" contentType="text/html charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=ISO-8859-1">
<title>Patient Profiles</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
<!-- <p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p> -->
<%-- <br/><p>patient${patientIDSession}</p> --%>
<%--  ${patientByID.PatientID}  --%>

<%-- <form:form method="POST" commandName="patientReg" action="doctorModProfile" modelAttribute="patient"> --%>

Patient Basic Details<br/><br/>
<form method="POST" action="home">

       <table border="1" style="width:100%">
       <tr><td><p>First Name</p></td><td><p><input type="text" name="fname" value="${patient.fname}" required/></p></td> 
       <tr><td><p>Last Name</p></td><td><p><input type="text" name="lname" value="${patient.lname}" required/></p></td>
       </table>
       <br/>
       <br/>
       Patient Address<br/><br/>
       <table border="1" style="width:100%">
       <tr><td><p>Street</p></td><td><p><input type="text" name="street" value="${patient.street}" required></p></td>
       <tr><td><p>Apartment No</p></td><td><p><input type="text" name="apt" value="${patient.apt}" required></p></td>
       <tr><td><p>City</p></td><td><p><input type="text" name="city" value="${patient.city}" required></p></td>
       <tr><td><p>Zipcode</p></td><td><p><input type="text" name="zipcode" value="${patient.zipcode}" required></p></td>
       <tr><td><p>Country</p></td><td><p><input type="text" name="country" value="${patient.country}" required></p></td>
       </table><br/><br/>
       Patient Contact Details<br/><br/>
       <table border="1" style="width:100%">
       <tr><td><p>Home Number</p></td><td><p><input type="text" name="homeNumber" value="${patient.homeNumber}" ></p></td>
       <tr><td><p>Cell Number</p></td><td><p><input type="text" name="cellNumber" value="${patient.cellNumber}" ></p> </td>
       <tr><td><p></p></td><td><input type="submit" name="submit" value="Update"/> </td>
       </table>
       <br/><br/>
       
       Patient Medicinal Details<br/><br/>
       <table border="1" style="width:100%">
       <tr><td><p>Allergies</p></td><td><p>${patient.allergies}</p></td>
       <tr><td><p>Medicinal Allergies</p></td><td><p>${patient.medicinalAllergies}</p></td>
       <tr><td><p>Disease</p></td><td><p>${patient.disease}</p></td>
       </table>
       
       <br/><br/>
       </form>
<%--   </form:form> --%>
       
       <%-- <a href="createPatientProfiles">Create a new Patient Profile</a><br/><br/>
       
       <input type="hidden" name="hiddenpatientid" value="${patientIDforref} }" /> --%>
       
Patient Profile<br/><br/>

<form:form method="POST" commandName="doctorMod" action="doctorModProfile" modelAttribute="patientProfile">
<table border="1" style="width:100%">
       <tr><td><p>Profile ID</p></td><td><p>${patientProfile.patientProfileID}</p></td>
       <tr><td><p>Prescription</p></td><td><p>${patientProfile.prescription}</p></td>
       <tr><td><p>Comments</p></td><td><p>${patientProfile.comments}</p></td>
       <tr><td><p>Diagnosis</p></td><td><p>${patientProfile.diagnosis}</p></td>
       <tr><td><p>Family History</p></td><td><p>${patientProfile.familyHistory}</p></td>
       <tr><td><p>Blood Group</p></td><td><p>${patientProfile.bloodGroup}</p></td>
       <!-- <tr><td><p></p></td><td><input type="submit" value="Update" /></td> -->
       </table>
       </form:form>
<br/><br/> 

<%--  Add new entry to Patient Profile<br/><br/>
<form:form name="insertData" method="POST" commandName="doctorMod" action="doctorInsertNewProfile" modelAttribute="patientProfile"> 
<table border="1" style="width:100%">
       <tr><td><p>Profile ID</p></td><td><form:input path="patientProfileID" /></td>
       <tr><td><p>Prescription</p></td><td><form:input path="prescription" /></td>
       <tr><td><p>Comments</p></td><td><form:input path="comments" /></td>
       <tr><td><p>Diagnosis</p></td><td><form:input path="diagnosis" /></td>
       <tr><td><p>Family History</p></td><td><form:input path="familyHistory" /></td>
       <tr><td><p>Blood Group</p></td><td><form:input path="bloodGroup" /></td>
       <tr><td><p></p></td><td><input type="submit" value="Update" /></td>
       </table>
       </form:form>
<br/><br/>  --%>

Lab Test Details
<br/><br/>
       <c:forEach var="labTests" items="${labTests}">
       <table border="1" style="width:100%">
              <tr><td><p>Lab Test ID</p></td><td><p>${labTests.labTestID}</p></td>
       <tr><td><p>Respiratory Rate</p></td><td><p>${labTests.respiratoryRate}</p></td>
       <tr><td><p>Heart Rate</p></td><td><p>${labTests.heartRate}</p></td>
       <tr><td><p>Blood Pressure</p></td><td><p>${labTests.bloodPressure}</p></td>
       <tr><td><p>Weight</p></td><td><p>${labTests.weight}</p></td>
       <tr><td><p>Blood Test</p></td><td><p>${labTests.bloodTest}</p></td>
       <tr><td><p>Cholesterol Level</p></td><td><p>${labTests.cholesterolLevel}</p></td>
       <tr><td><p>Protein Level</p></td><td><p>${labTests.proteinLevel}</p></td>
       <tr><td><p>Hemoglobin</p></td><td><p>${labTests.hemoglobin}</p></td>
       <tr><td><p>Current Date</p></td><td><p>${labTests.currentDate}</p></td>
       </table>
              <br/><br/>
       </c:forEach>

</body>
</html>
