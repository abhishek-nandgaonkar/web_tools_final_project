<%@ page language="java" contentType="text/html charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=ISO-8859-1">
<title>Patient Profiles</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
<p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
<%-- <br/><p>patient</p>
 ${patientByID.PatientID} 
Patient Basic Details<br/><br/>
	
	${patientByID.fname}<br/>
	${patientByID.lname}<br/>
	<br/><br/>
	Patient Address<br/><br/>
	${patientByID.street}<br/>
	${patientByID.apt}<br/>
	${patientByID.city}<br/>
	${patientByID.zipcode}<br/>
	${patientByID.country}<br/>
	
	Patient Contact Details<br/><br/>
		${patientByID.homeNumber}<br/>
	${patientByID.cellNumber} <br/>
	<br/><br/>
	Patient Medicinal Details<br/><br/>
	${patientByID.allergies}<br/>
	${patientByID.medicinalAllergies}<br/>
	${patientByID.disease}<br/>

<p>patient profile</p>
	${patientProfile.patientProfileID}<br/>
	${patientProfile.prescription}<br/>
	${patientProfile.comments}<br/>
	${patientProfile.diagnosis}<br/>
	${patientProfile.familyHistory}<br/>
	${patientProfile.bloodGroup}<br/>
<br/><br/>
lab test data
<br/><br/>
	<c:forEach var="labTests" items="${labTests}">
		${labTests.labTestID}<br/>
	${labTests.respiratoryRate}<br/>
	${labTests.heartRate}<br/>
	${labTests.bloodPressure}<br/>
	${labTests.weight}<br/>
	${labTests.bloodTest}<br/>
	${labTests.cholesterolLevel}<br/>
	${labTests.proteinLevel}<br/>
	${labTests.hemoglobin}<br/>
	${labTests.currentDate}<br/>
		<br/><br/>
	</c:forEach>
 --%>
 Patient Details<br/><br/>
 <c:forEach var="patientListUsername" items="${patientListUsername}">
  
Username: ${patientListUsername.username}<br/>
First Name: ${patientListUsername.fname}<br/>
Last Name: ${patientListUsername.lname}<br/>
Street: ${patientListUsername.street}<br/>
Apartment Number: ${patientListUsername.apt}<br/>
City: ${patientListUsername.city}<br/>
Zipcode: ${patientListUsername.zipcode}<br/>
Country: ${patientListUsername.country}<br/>
Hpme Number${patientListUsername.homeNumber}<br/>
Cell Number: ${patientListUsername.cellNumber}<br/><br/>
   <br/>
 </c:forEach>
</body>
</html>