<%@ page language="java" contentType="text/html charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=ISO-8859-1">
<title>Patient Profiles</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
<p><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
 Add new entry to Patient Profile ${patientIDSession} <br/><br/>
<form method="POST" action="doctorInsertNewProfile" modelAttribute="patientProfile"> 
<table border="1" style="width:100%">
	<tr><td>Profile ID</td><td><input name="patientProfileID" value="${patientIDSession}" /></td> 
	<tr><td>Prescription</td><td><input name="prescription" type="text"/></td>
	<tr><td>Comments</td><td><input name="comments" type="text"/></td>
	<tr><td>Diagnosis</td><td><input name="diagnosis" type="text"/></td>
	<tr><td>Family History</td><td><input name="familyHistory" type="text" /></td>
	<tr><td>Blood Group</td><td><input name="bloodGroup" type="text"/></td>
	<tr><td></td><td><input type="submit" value="Insert Data" /></td>
	</table>
	</form>
<br/><br/>  

</body>
</html>