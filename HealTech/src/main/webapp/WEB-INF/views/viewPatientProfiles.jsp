<%@ page language="java" contentType="text/html charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=ISO-8859-1">
<title>Patient Profiles</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>
<p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
<%-- <br/><p>patient</p>
 ${patientByID.PatientID} 
Patient Basic Details<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>First Name</td><td>${patientByID.fname}</td>
	<tr><td>Last Name</td><td>${patientByID.lname}</td>
	</table>
	<br/>
	<br/>
	Patient Address<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>Street</td><td>${patientByID.street}</td>
	<tr><td>Apartment No</td><td>${patientByID.apt}</td>
	<tr><td>City</td><td>${patientByID.city}</td>
	<tr><td>Zipcode</td><td>${patientByID.zipcode}</td>
	<tr><td>Country</td><td>${patientByID.country}</td>
	</table><br/><br/>
	Patient Contact Details<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>Home Number</td><td>${patientByID.homeNumber}</td>
	<tr><td>Cell Number</td><td>${patientByID.cellNumber} </td>
	</table>
	<br/><br/>
	
	Patient Medicinal Details<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>Profile ID</td><td>${patientByID.allergies}</td>
	<tr><td>Profile ID</td><td>${patientByID.medicinalAllergies}</td>
	<tr><td>Profile ID</td><td>${patientByID.disease}</td>
	</table>
	
	<br/><br/>
	
	<a href="viewPatientProfiles">View Existing Patient Profiles</a>
	<a href="createPatientProfiles">Create a new Patient Profile</a>
	 --%>
Patient Profile<br/><br/>
<form method="POST" action="doctorModProfile" modelAttribute="patientProfile">
<table border="1" style="width:100%">
	<tr><td>Profile ID</td><td><input type="patientProfileID" value="${patientProfile.patientProfileID}" required/></td>
	<tr><td>Prescription</td><td><input type="prescription" value="${patientProfile.prescription}" required/></td>
	<tr><td>Comments</td><td><input type="comments" value="${patientProfile.comments}" required/></td>
	<tr><td>Diagnosis</td><td><input type="diagnosis" value="${patientProfile.diagnosis}" required/></td>
	<tr><td>Family History</td><td><input type="familyHistory" value="${patientProfile.familyHistory}" required/></td>
	<tr><td>Blood Group</td><td><input type="bloodGroup" value="${patientProfile.bloodGroup}" required/></td>
	<tr><td></td><td><input type="submit" value="Update" /></td>
</table>
	</form>
<br/><br/>

<%--  Add new entry to Patient Profile<br/><br/>
<form:form name="insertData" method="POST" commandName="doctorMod" action="doctorInsertNewProfile" modelAttribute="patientProfile">
<table border="1" style="width:100%">
	<tr><td>Profile ID</td><td><form:input path="patientProfileID" /></td>
	<tr><td>Prescription</td><td><form:input path="prescription" /></td>
	<tr><td>Comments</td><td><form:input path="comments" /></td>
	<tr><td>Diagnosis</td><td><form:input path="diagnosis" /></td>
	<tr><td>Family History</td><td><form:input path="familyHistory" /></td>
	<tr><td>Blood Group</td><td><form:input path="bloodGroup" /></td>
	<tr><td></td><td><input type="submit" value="Update" /></td>
	</table>
	</form:form>
<br/><br/> 

Lab Test Details
<br/><br/>
	<c:forEach var="labTests" items="${labTests}">
	<table border="1" style="width:100%">
		<tr><td>Profile ID</td><td>${labTests.labTestID}</td>
	<tr><td>Profile ID</td><td>${labTests.respiratoryRate}</td>
	<tr><td>Profile ID</td><td>${labTests.heartRate}</td>
	<tr><td>Profile ID</td><td>${labTests.bloodPressure}</td>
	<tr><td>Profile ID</td><td>${labTests.weight}</td>
	<tr><td>Profile ID</td><td>${labTests.bloodTest}</td>
	<tr><td>Profile ID</td><td>${labTests.cholesterolLevel}</td>
	<tr><td>Profile ID</td><td>${labTests.proteinLevel}</td>
	<tr><td>Profile ID</td><td>${labTests.hemoglobin}</td>
	<tr><td>Profile ID</td><td>${labTests.currentDate}</td>
	</table>
		<br/><br/>
	</c:forEach>
 --%>
</body>
</html>