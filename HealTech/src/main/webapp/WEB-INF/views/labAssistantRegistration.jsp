<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix = "form" %>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body bgcolor="#BDFCC9">
<p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
<form:form method="POST" commandName="labAssistantReg" action="labAssistantRegistrationSuccessful" modelAttribute="labAssistant" >
		<table align="center">
			<tr>
				<td><b> Quick Search</b></td>
			</tr>
			
				<tr><td>Username: <form:input path="username" /><form:errors path="username"></form:errors></td></tr>
				<tr><td>Password: <form:input path="password" /><form:errors path="password"></form:errors></td></tr>
				<tr><td>Department: <form:input path="department" /><form:errors path="department"></form:errors></td></tr>
				<tr><td>EmployeeID: <form:input path="employeeID" /><form:errors path="employeeID"></form:errors></td></tr>
			
			<tr>
				<td colspan="3"><input type="submit" value="Submit Changes" /></td>
			</tr>
		</table>
	</form:form>


</body>
</html>