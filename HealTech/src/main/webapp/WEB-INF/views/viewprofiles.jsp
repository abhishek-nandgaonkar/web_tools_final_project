<%@ page language="java" contentType="text/html charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=ISO-8859-1">
<title>Patient Profiles</title>
<style type="text/css">
<%@include file = "css\csssheet.css"%>
</style>
</head>
<body>

<p align = "center"><img alt="FaceBuy" height="96" longdesc="FaceBuy" align="center" src="images/healtech.PNG" width="307" /></p>
<<!-- p align = "right"><a href ="printProfile"> Print Profile </a></p>  -->
	<%-- <form method ="post" action = "printProfile">
	 <input type ="submit" name="submit" value = "Print Profile">
		</form>  --%>
<br/>
<%--  ${patientByID.PatientID}  --%>

Patient Basic Details<br/><br/>
<form method ="post" action = "printProfile"> 
	<table border="1" style="width:100%">
	<tr><td class = "print">First Name</td><td><input type="text" name ="street" value ="${patientByID.fname}" readonly/></td>
	
	<tr><td>Last Name</td><td><input type="text" name ="street" value ="${patientByID.lname}" readonly/></td>
	</table>
	<br/>
	<br/>
	Patient Address<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>Street</td><td><input type="text" name ="street" value ="${patientByID.street}" readonly/>   </td>
	<tr><td>Apartment No</td><td><input type="text" name ="street" value ="${patientByID.apt}" readonly/></td>
	<tr><td>City</td><td><input type="text" name ="street" value ="${patientByID.city}" readonly/> </td>
	<tr><td>Zipcode</td><td><input type="text" name ="street" value ="${patientByID.zipcode}" readonly/> </td>
	<tr><td>Country</td><td><input type="text" name ="street" value ="${patientByID.country}" readonly/> </td>
	</table><br/><br/>
	Patient Contact Details<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>Home Number</td><td><input type="text" name ="street" value ="${patientByID.homeNumber}" readonly/> </td>
	<tr><td>Cell Number</td><td><input type="text" name ="street" value ="${patientByID.cellNumber}" readonly/>  </td>
	</table>
	<br/><br/>
	
	<%-- <input type ="submit" name="submit" value = "Print Profile">
		</form>  --%>
	
	Patient Medicinal Details<br/><br/>
	<table border="1" style="width:100%">
	<tr><td>Profile ID</td><td><input type="text" name ="street" value ="${patientByID.allergies}" readonly/></td>
	<tr><td>Profile ID</td><td><input type="text" name ="street" value ="${patientByID.medicinalAllergies}" readonly/></td>
	<tr><td>Profile ID</td><td><input type="text" name ="street" value ="${patientByID.disease}" readonly/></td>
	</table>
	
	<br/><br/>
	<input type ="submit" name="submit" value = "Print Profile">
		</form> 
	
	<a href="createPatientProfiles">Create a new Patient Profile</a><br/><br/>
	
	<input type="hidden" name="hiddenpatientid" value="${patientIDforref} }" />
	
Patient Profile<br/><br/>

			
			
<form:form method="POST" commandName="doctorMod" action="doctorModProfile" modelAttribute="patientProfile">

<table border="1" style="width:100%">

	<tr><td>Profile ID</td><td><form:input path="patientProfileID" value="${patientProfile.patientProfileID}"/></td>
	<tr><td>Prescription</td><td><form:input path="prescription" value="${patientProfile.prescription}"/></td>
	<tr><td>Comments</td><td><form:input path="comments" value="${patientProfile.comments}"/></td>
	<tr><td>Diagnosis</td><td><form:input path="diagnosis" value="${patientProfile.diagnosis}"/></td>
	<tr><td>Family History</td><td><form:input path="familyHistory" value="${patientProfile.familyHistory}"/></td>
	<tr><td>Blood Group</td><td><form:input path="bloodGroup" value="${patientProfile.bloodGroup}"/></td>
	<tr><td></td><td><input type="submit" value="Update" /></td>
	</table>
	</form:form>
<br/><br/> 

<%--  Add new entry to Patient Profile<br/><br/>
<form:form name="insertData" method="POST" commandName="doctorMod" action="doctorInsertNewProfile" modelAttribute="patientProfile"> 
<table border="1" style="width:100%">
	<tr><td>Profile ID</td><td><form:input path="patientProfileID" /></td>
	<tr><td>Prescription</td><td><form:input path="prescription" /></td>
	<tr><td>Comments</td><td><form:input path="comments" /></td>
	<tr><td>Diagnosis</td><td><form:input path="diagnosis" /></td>
	<tr><td>Family History</td><td><form:input path="familyHistory" /></td>
	<tr><td>Blood Group</td><td><form:input path="bloodGroup" /></td>
	<tr><td></td><td><input type="submit" value="Update" /></td>
	</table>
	</form:form>
<br/><br/>  --%>

Lab Test Details
<br/><br/>
	<c:forEach var="labTests" items="${labTests}">
	<table border="1" style="width:100%">
		<tr><td>Lab Test ID</td><td>${labTests.labTestID}</td>
	<tr><td>Respiratory Rate</td><td>${labTests.respiratoryRate}</td>
	<tr><td>Heart Rate</td><td>${labTests.heartRate}</td>
	<tr><td>Blood Pressure</td><td>${labTests.bloodPressure}</td>
	<tr><td>Weight</td><td>${labTests.weight}</td>
	<tr><td>Blood Test</td><td>${labTests.bloodTest}</td>
	<tr><td>Cholesterol Level</td><td>${labTests.cholesterolLevel}</td>
	<tr><td>Protein Level</td><td>${labTests.proteinLevel}</td>
	<tr><td>Hemoglobin</td><td>${labTests.hemoglobin}</td>
	<tr><td>Current Date</td><td>${labTests.currentDate}</td>
	</table>
		<br/><br/>
	</c:forEach>

</body>
</html>