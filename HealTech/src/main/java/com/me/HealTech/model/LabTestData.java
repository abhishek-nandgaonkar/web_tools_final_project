package com.me.HealTech.model;

import java.io.Serializable;

public class LabTestData implements Serializable
{
	private int labTestID;
	private float respiratoryRate;
	private float heartRate;
	private float bloodPressure;
	private float weight;
	private String bloodTest;
	private String cholesterolLevel;
	private String proteinLevel;
	private String hemoglobin;
	private String currentDate;
	
	private LabAssistant labAssistant;
	
	public LabAssistant getLabAssistant() {
		return labAssistant;
	}
	public void setLabAssistant(LabAssistant labAssistant) {
		this.labAssistant = labAssistant;
	}
	public int getLabTestID() {
		return labTestID;
	}
	public void setLabTestID(int labTestID) {
		this.labTestID = labTestID;
	}
	public String getBloodTest() {
		return bloodTest;
	}
	public void setBloodTest(String bloodTest) {
		this.bloodTest = bloodTest;
	}
	public String getCholesterolLevel() {
		return cholesterolLevel;
	}
	public void setCholesterolLevel(String cholesterolLevel) {
		this.cholesterolLevel = cholesterolLevel;
	}
	public String getProteinLevel() {
		return proteinLevel;
	}
	public void setProteinLevel(String proteinLevel) {
		this.proteinLevel = proteinLevel;
	}
	public String getHemoglobin() {
		return hemoglobin;
	}
	public void setHemoglobin(String hemoglobin) {
		this.hemoglobin = hemoglobin;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	private Patient patient;
	
	/*public int getPatientID() {
		return patientID;
	}
	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}*/
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public float getRespiratoryRate() {
		return respiratoryRate;
	}
	public void setRespiratoryRate(float respiratoryRate) {
		this.respiratoryRate = respiratoryRate;
	}
	public float getHeartRate() {
		return heartRate;
	}
	public void setHeartRate(float heartRate) {
		this.heartRate = heartRate;
	}
	public float getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(float bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	

}
