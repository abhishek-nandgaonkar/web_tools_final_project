package com.me.HealTech.model;

import java.io.Serializable;
import java.util.Set;

public class LabAssistant implements Serializable
{
	private int employeeID;
	private String department;
	private String username;
	private String password;
	
	private Set<LabTestData> labTestData;
	public int getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
	
	public Set<LabTestData> getLabTestData() {
		return labTestData;
	}
	public void setLabTestData(Set<LabTestData> labTestData) {
		this.labTestData = labTestData;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	

}
