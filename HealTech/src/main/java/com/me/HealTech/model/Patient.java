package com.me.HealTech.model;

import java.io.Serializable;
import java.util.Set;

public class Patient implements Serializable
{
	private int PatientID;
	private String username;
	private String password;
	private String country;
	private String fname;
	private String lname;
	private String street;
	private String apt;
	private String city;
	private String zipcode;
	//private int preferredDoctor;
	private String allergies;
	private String medicinalAllergies;
	private String disease;
	private String homeNumber;
	private String cellNumber;
	
	
	public Set<PatientProfile> getPatientProfile() {
		return patientProfile;
	}
	public void setPatientProfile(Set<PatientProfile> patientProfile) {
		this.patientProfile = patientProfile;
	}
	public Set<LabTestData> getLabTestData() {
		return labTestData;
	}
	public void setLabTestData(Set<LabTestData> labTestData) {
		this.labTestData = labTestData;
	}
	private Doctor prefferedDoctor;
	private Set<PatientProfile> patientProfile;
	private Set<LabTestData> labTestData;
	
	public String getAllergies() {
		return allergies;
	}
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	public String getMedicinalAllergies() {
		return medicinalAllergies;
	}
	public void setMedicinalAllergies(String medicinalAllergies) {
		this.medicinalAllergies = medicinalAllergies;
	}
	public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}
	private Citizen citizen;
	
	
	
	public Citizen getCitizen() {
		return citizen;
	}
	public void setCitizen(Citizen citizen) {
		this.citizen = citizen;
	}

	public int getPatientID() {
		return PatientID;
	}
	public void setPatientID(int patientID) {
		PatientID = patientID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/*public int getPreferredDoctor() {
		return preferredDoctor;
	}
	public void setPreferredDoctor(int preferredDoctor) {
		this.preferredDoctor = preferredDoctor;
	}*/
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getApt() {
		return apt;
	}
	public void setApt(String apt) {
		this.apt = apt;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getHomeNumber() {
		return homeNumber;
	}
	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public Doctor getPrefferedDoctor() {
		return prefferedDoctor;
	}
	public void setPrefferedDoctor(Doctor prefferedDoctor) {
		this.prefferedDoctor = prefferedDoctor;
	}
	
	

}
