package com.me.HealTech.model;

import java.io.Serializable;

public class PatientProfile implements Serializable
{
	private int patientProfileID;
	
	private String prescription;
	private String comments;
	private String diagnosis;
	private String familyHistory;
	private String bloodGroup;
	
	public String getBloodGroup() {
		return bloodGroup;
	}
	public int getPatientProfileID() {
		return patientProfileID;
	}
	public void setPatientProfileID(int patientProfileID) {
		this.patientProfileID = patientProfileID;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getFamilyHistory() {
		return familyHistory;
	}
	public void setFamilyHistory(String familyHistory) {
		this.familyHistory = familyHistory;
	}
	private Patient patient;
	
	/*public int getPatientID() {
		return patientID;
	}
	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}*/
	
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public String getPrescription() {
		return prescription;
	}
	public void setPrescription(String prescription) {
		this.prescription = prescription;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	
	
	

}
