package com.me.HealTech.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import com.me.HealTech.model.Patient;



public class PatientValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Patient.class.equals(clazz);
		
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		Patient patient = (Patient) target;

		
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "PatientID",
				"validate.PatientID", "Please check the username entered");*/
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username",
				"validate.username", "Please check the username entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
				"validate.password", "Please check the password entered");
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fname",
				"validate.fname", "Please check the fname entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lname",
				"validate.lname", "Please check the lname entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "street",
				"validate.street", "Please check the street entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apt",
				"validate.apt", "Please check the apt entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city",
				"validate.city", "Please check the city entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipcode",
				"validate.zipcode", "Please check the zipcode entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "allergies",
				"validate.allergies", "Please check the allergies entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "medicinalAllergies",
				"validate.medicinalAllergies", "Please check the Medicinal Allergy entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "disease",
				"validate.disease", "Please check the disease entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country",
				"validate.country", "Please check the country entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "homeNumber",
				"validate.homeNumber", "Please check the homeNumber entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cellNumber",
				"validate.cellNumber", "Please check the cellNumber entered");*/
	}

}
