package com.me.HealTech.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import com.me.HealTech.model.LabAssistant;



public class LabAssistantValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return LabAssistant.class.equals(clazz);
		
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		LabAssistant labAssistant = (LabAssistant) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username",
				"validate.username", "Please check the username entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
				"validate.password", "Please check the password entered");
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "department",
				"validate.department", "Please check the department entered");*/

	}

}
