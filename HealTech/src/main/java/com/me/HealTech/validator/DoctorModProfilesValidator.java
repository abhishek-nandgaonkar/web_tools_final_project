package com.me.HealTech.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;




import com.me.HealTech.model.Patient;
import com.me.HealTech.model.PatientProfile;



public class DoctorModProfilesValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return PatientProfile.class.equals(clazz);
		
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		PatientProfile patientProfile = (PatientProfile) target;
		
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "PatientID",
				"validate.PatientID", "Please check the username entered");*/
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "patientProfileID","validate.patientProfileID", "Please check the patientProfileID entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "prescription","validate.prescription", "Please check the prescription entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "comments","validate.comments", "Please check the comments entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "diagnosis","validate.diagnosis", "Please check the diagnosis entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "familyHistory","validate.familyHistory", "Please check the familyHistory entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bloodGroup","validate.bloodGroup", "Please check the bloodGroup entered");
		
		
		
		
		
	}

}
