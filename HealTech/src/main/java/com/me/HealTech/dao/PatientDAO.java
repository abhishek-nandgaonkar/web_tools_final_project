package com.me.HealTech.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.me.HealTech.model.Citizen;
import com.me.HealTech.model.Doctor;
import com.me.HealTech.model.LabTestData;
import com.me.HealTech.model.Patient;
import com.me.HealTech.model.PatientProfile;




public class PatientDAO extends DAO
{

	public Patient queryUserByNameAndPassword(String username, String password)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from PatientDAO");
			System.out.println("patient credentials" + username + " " +password);
			Query q = getSession()
					.createQuery(
							"from Patient where username = :username and password = :password");
			q.setString("username", username);
			q.setString("password", password);
			System.out.println("query from patientDao: " + q);
			Patient patient = (Patient) q.uniqueResult();
			System.out.println("patient in empdao" + patient);
			// commit();
			return patient;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public Patient queryUserByID(int patientID)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from PatientDAO");
			//System.out.println("patient credentials" + username + " " +password);
			Query q = getSession()
					.createQuery(
							"from Patient where patientID = :patientID");
			q.setInteger("patientID", patientID);
			//q.setString("password", password);
			System.out.println("query from patientDao: " + q);
			Patient patient = (Patient) q.uniqueResult();
			System.out.println("patient in empdao" + patient);
			// commit();
			return patient;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " +  e);
		}
	}

	public List<Patient> queryUserByUsername(String username)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from PatientDAO");
			System.out.println("patient credentials" + username );
			Query q = getSession()
					.createQuery(
							"from Patient where username like :username ");
			q.setString("username", "%"+username+"%");
			/*           q.setString("username", username);*/
			//q.setString("password", password);
			System.out.println("query from patientDao: " + q);
			List<Patient> patientList= (List) q.list();
			//System.out.println("patient in empdao" + patient);
			// commit();
			return patientList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public Patient queryOneUserByUsername(String username)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from PatientDAO");
			System.out.println("patient credentials" + username );
			Query q = getSession()
					.createQuery(
							"from Patient where username = :username ");
			q.setString("username", username);
			/*           q.setString("username", username);*/
			//q.setString("password", password);
			System.out.println("query from patientDao: " + q);
			Patient pat = (Patient) q.uniqueResult();

			System.out.println("patient in empdao" + pat);
			// commit();
			return pat;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public List<Patient> quickQuery(String username, String fname, String lname, String city, String country, String state)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from PatientDAO");
			System.out.println("patient credentials" + username );
			Query q = getSession()
					.createQuery(
							"from Patient where username like :username and fname like :fname and lname like :lname and city like :city"
									+ "country like :country and state like :state");
			q.setString("username", username);
			q.setString("fname", fname);
			q.setString("lname", lname);
			q.setString("city", city);
			q.setString("country", country);
			q.setString("state", state);

			//q.setString("password", password);
			System.out.println("query from patientDao: " + q);
			List<Patient> patientList= (List) q.list();
			//System.out.println("patient in empdao" + patient);
			// commit();
			return patientList;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public void insertPatient(int patientID, String username, String password, String country, String fname, String lname,
			String street, String apt, String city, String zipcode, String homeNumber, String cellNumber) throws Exception
	{                              
		try
		{
			Session session = getSession().getSessionFactory().openSession();
			session.beginTransaction();

			Patient patient = new Patient();
			patient.setPatientID(patientID);
			patient.setUsername(username);
			patient.setPassword(password);
			patient.setCountry(country);
			patient.setFname(fname);
			patient.setLname(lname);
			patient.setStreet(street);
			patient.setApt(apt);
			patient.setCity(city);
			patient.setZipcode(zipcode);
			patient.setHomeNumber(homeNumber);
			patient.setCellNumber(cellNumber);

			patient.setPrefferedDoctor(new DoctorDAO().getDoctorByID(0));

			session.save(patient);
			session.getTransaction().commit();
		}
		catch(HibernateException hex)
		{
			hex.printStackTrace();
		}

	}


	public List<Patient> queryByDoctorID(int doctorID) throws Exception
	{
		try{
			System.out.println("printing from patientDAO querybydoctorid");
			Query q = getSession().createQuery("from Patient where doctorID = :doctorID");
			System.out.println("q: " + q);
			q.setInteger("doctorID", doctorID);
			System.out.println("doctorid:" + doctorID);

			List<Patient> patientList = q.list();
			System.out.println("patient list:" + patientList);
			return patientList;
		}
		catch(HibernateException he)
		{
			he.printStackTrace();
			System.out.println(he.getMessage());
		}
		return null;
	}

}
