package com.me.HealTech.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.me.HealTech.model.LabAssistant;
import com.me.HealTech.model.Patient;



public class LabAssistantDAO extends DAO
{
	
	public LabAssistant queryUserByNameAndPassword(String username, String password)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from LabAssistantDAO");
			System.out.println("labAssistant credentials" + username + " " +password);
			Query q = getSession()
					.createQuery(
							"from LabAssistant where username = :username and password = :password");
			q.setString("username", username);
			q.setString("password", password);
			System.out.println("query from labAssistantDao: " + q);
			LabAssistant labAssistant = (LabAssistant) q.uniqueResult();
			System.out.println("labAssistant in empdao" + labAssistant);
			// commit();
			return labAssistant;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}
	
	public void insertLabAssistant(int employeeID, String department, String username, String password) throws Exception
	{		
		try
		{
			System.out.println("printing from insert lab assistant");
			Session session = getSession().getSessionFactory().openSession();
			session.beginTransaction();

			LabAssistant labAssistant = new LabAssistant();
			labAssistant.setEmployeeID(employeeID);
			labAssistant.setDepartment(department);
			labAssistant.setUsername(username);
			labAssistant.setPassword(password);
			System.out.println("labassistant:" + labAssistant);
			

			/*patient.setPrefferedDoctor(new DoctorDAO().getDoctorByID(0));*/
			
			session.save(labAssistant);
			session.getTransaction().commit();
		}
		catch(HibernateException hex)
		{
			hex.printStackTrace();
		}
	}
}
