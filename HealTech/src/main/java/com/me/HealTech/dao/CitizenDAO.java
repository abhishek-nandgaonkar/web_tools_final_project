package com.me.HealTech.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.HealTech.model.Citizen;



public class CitizenDAO extends DAO
{
	
	public Citizen queryUserByNameAndPassword(String username, String password)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from CitizenDAO");
			System.out.println("citizen credentials" + username + " " +password);
			Query q = getSession()
					.createQuery(
							"from Citizen where username = :username and password = :password");
			q.setString("username", username);
			q.setString("password", password);
			System.out.println("query from citizenDao: " + q);
			Citizen citizen = (Citizen) q.uniqueResult();
			System.out.println("citizen in empdao" + citizen);
			// commit();
			return citizen;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}
}
