package com.me.HealTech.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.me.HealTech.model.LabAssistant;
import com.me.HealTech.model.Patient;
import com.me.HealTech.model.PatientProfile;


public class PatientProfileDAO extends DAO
{
	public PatientProfile getPatientProfile(int patientID)	throws Exception
	{
		try {

			Query q = getSession().createQuery("from PatientProfile where patientProfileID = :patientID");
			q.setInteger("patientID", patientID);

			PatientProfile patientProfile = (PatientProfile) q.uniqueResult();

			return patientProfile;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user ");
		}

	}

	public int getMaxID()
	{
		try
		{
			Query q = getSession().createQuery("select max(patientProfileID) from PatientProfile");
			int max = (Integer) q.uniqueResult();
			return max;
		}
		catch(HibernateException e)
		{

		}
		return 0;
	}

	public void insertNewPatientProfile(int patientID, String prescription, String comments, String diagnosis,
			String familyHistory, String bloodGroup) throws Exception
	{
		try
		{
			System.out.println("printing from insert new patient profile");
			Session session = getSession().getSessionFactory().openSession();
			session.beginTransaction();

			int maxID = getMaxID();
			PatientProfile patientProfile = new PatientProfile();
			System.out.println("patientProfile" + patientProfile);
			System.out.println("maxID: " + maxID + prescription + comments + diagnosis);
			Patient patient = new Patient();
			patient.setPatientID(patientID);
			patientProfile.setPatient(patient);
			patientProfile.setBloodGroup(bloodGroup);
			patientProfile.setComments(comments);
			patientProfile.setDiagnosis(diagnosis);
			patientProfile.setFamilyHistory(familyHistory);
			patientProfile.setPatientProfileID(maxID+1);
			patientProfile.setPrescription(prescription);
			System.out.println("done patient profile dao");
			session.save(patientProfile);
			session.getTransaction().commit();
		}
		catch(HibernateException e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}


	}


	public void updatePatientProfile(int patientProfileID, String prescription, String comments, String diagnosis,
			String familyHistory, String bloodGroup) throws Exception
	{
		try
		{

			System.out.println("printing from update patient profile");
			int maxID = getMaxID();
			System.out.println("maxID" + maxID);
			Query q = getSession().createQuery("update PatientProfile set prescription = :prescription "
					+ ", comments = :comments, "
					+ "diagnosis = :diagnosis, familyHistory = :familyHistory, bloodGroup = :bloodGroup "
					+ "where patientProfileID = :patientProfileID");
			System.out.println("query" + prescription + comments + diagnosis  + familyHistory + bloodGroup + patientProfileID);
			
			q.setParameter("prescription", prescription);
			q.setParameter("comments", comments);
			q.setParameter("diagnosis", diagnosis);
			q.setParameter("familyHistory", familyHistory);
			q.setParameter("bloodGroup", bloodGroup);
			q.setParameter("patientProfileID", patientProfileID);
			System.out.println("query" + q);

			int result = q.executeUpdate();
			System.out.println("result" + result);

		}
		catch(HibernateException e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

	public int getPatientID(int patientProfileID) {
		// TODO Auto-generated method stub
		try{
		Query q = getSession().createQuery("select PatientID from PatientProfile where patientProfileID = :patientProfileID");
		q.setInteger("patientProfileID", patientProfileID);
		int max = (Integer) q.uniqueResult();
		return max;
		}
		catch(HibernateException e)
		{
			
		}
		return 0;
	}
}
