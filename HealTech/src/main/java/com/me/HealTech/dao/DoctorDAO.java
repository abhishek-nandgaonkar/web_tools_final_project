package com.me.HealTech.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.me.HealTech.model.Doctor;
import com.me.HealTech.model.Patient;

public class DoctorDAO extends DAO
{

	public Doctor queryUserByNameAndPassword(String username, String password)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from EmployeeDAO");
			System.out.println("employee credentials" + username + " " +password);
			Query q = getSession()
					.createQuery(
							"from Doctor where username = :username and password = :password");
			q.setString("username", username);
			q.setString("password", password);
			System.out.println("query from employeeDao: " + q);
			Doctor doctor = (Doctor) q.uniqueResult();
			System.out.println("employee in empdao" + doctor);
			// commit();
			return doctor;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public Doctor getDoctorByID(int doctorID) throws Exception
	{
		try {
			Query q = getSession().createQuery("from Patient where doctorID = :doctorID");
			q.setInteger("doctorID", doctorID);

			Doctor doctor = (Doctor) q.uniqueResult();

			return doctor;
		} catch (HibernateException e) {

			throw new Exception("Could not get user " +  e);
		}
	}

	public void insertDoctor(int doctorID, String username, String password, String license, String country, String fname, String lname,
			String street, String apt, String city, String zipcode, String homeNumber, String cellNumber, String specialty) throws Exception
	{                              
		try
		{
			Session session = getSession().getSessionFactory().openSession();
			session.beginTransaction();

			Doctor doctor = new Doctor();
			doctor.setDoctorID(doctorID);
			doctor.setSpecialty(specialty);
			doctor.setUsername(username);
			doctor.setLicense(license);
			doctor.setPassword(password);
			doctor.setCountry(country);
			doctor.setFname(fname);
			doctor.setLname(lname);
			doctor.setStreet(street);
			doctor.setApt(apt);
			doctor.setCity(city);
			doctor.setZipcode(zipcode);
			doctor.setHomeNumber(homeNumber);
			doctor.setCellNumber(cellNumber);


			session.save(doctor);
			session.getTransaction().commit();
		}
		catch(HibernateException hex)
		{
			hex.printStackTrace();
		}

	}


	public List<Doctor> queryUserByUsername(String username)
			throws Exception {
		try {
			Query q = getSession().createQuery("from Doctor where username = :username ");
			q.setString("username", username);
			System.out.println("query from doctorDao: " + q);
			List<Doctor> doctorList= (List) q.list();
			return doctorList;
		} catch (HibernateException e) {
			throw new Exception("Could not get user " + username, e);
		}
	}

	public List<Doctor> quickQuery(String username, String fname, String lname, String city, String country, String state)
			throws Exception {
		try {
			Query q = getSession().createQuery("from Doctor where username like :username and fname like :fname and lname like :lname and city like :city"
					+ "country like :country and state like :state");
			q.setString("username", username);
			q.setString("fname", fname);
			q.setString("lname", lname);
			q.setString("city", city);
			q.setString("country", country);
			q.setString("state", state);

			List<Doctor> doctorList= (List) q.list();

			return doctorList;
		} catch (HibernateException e) {
			throw new Exception("Could not get user " + username, e);
		}
	}

	public Doctor queryOneUserByUsername(String username)
			throws Exception {
		try {
			// begin();
			System.out.println("Printing from DoctorDAO");
			System.out.println("doctor credentials" + username );
			Query q = getSession()
					.createQuery(
							"from Doctor where username = :username ");
			q.setString("username", username);
			/*           q.setString("username", username);*/
			//q.setString("password", password);
			System.out.println("query from DoctorDAO: " + q);
			Doctor doctor = (Doctor) q.uniqueResult();

			System.out.println("patient in doctordao" + doctor);
			// commit();
			return doctor;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user " + username, e);
		}
	}

	public Doctor queryByDoctorID(int doctorID) throws Exception
	{
		try{
			System.out.println("printing from doctorDAO querybydoctorid");
			Query q = getSession().createQuery("from Doctor where doctorID = :doctorID");
			System.out.println("q: " + q);
			q.setInteger("doctorID", doctorID);
			System.out.println("doctorid:" + doctorID);

			Doctor doctor = (Doctor) q.uniqueResult();
			System.out.println("patient list:" + doctor);
			return doctor;
		}
		catch(HibernateException he)
		{
			he.printStackTrace();
			System.out.println(he.getMessage());
		}
		return null;
	}

	public void updateDoctor(String username, String password, String fname, String lname, String specialty, String license,
			String street, String apt, String city, String country, String homeNumber, String cellNumber)
	{
		Query q = getSession().createQuery("update Doctor set username = :username, password = :password,"
				+ "fname = :fname, lname=:lname, specialty = :specialty, license = :license, street = :street, apt = :apt, city = :city,"
				+ "country = :country, homeNumber = :homeNumber, cellNumber = :cellNumber");

		q.setString("username", username);
		q.setString("password", password);
		q.setString("fname", fname);
		q.setString("lname", lname);
		q.setString("specialty", specialty);
		q.setString("license", license);
		q.setString("street", street);
		q.setString("apt", apt);
		q.setString("city", city);
		q.setString("country", country);
		q.setString("homeNumber", homeNumber);
		q.setString("cellNumber", cellNumber);

		int result = q.executeUpdate();
		System.out.println("result: " + result);

	}

}
