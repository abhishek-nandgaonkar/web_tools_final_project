package com.me.HealTech.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.HealTech.model.LabAssistant;
import com.me.HealTech.model.LabTestData;
import com.me.HealTech.model.PatientProfile;


public class LabTestDataDAO extends DAO
{
	public List<LabTestData> getLabTests(int patientID)	throws Exception
	{
		try {
			System.out.println("printing from lab test data dao");
			Query q = getSession().createQuery("from LabTestData where patientID = :patientID");
			System.out.println(q);
			q.setInteger("patientID", patientID);
			
			@SuppressWarnings("unchecked")
			List<LabTestData> labTests = q.list();
			
			System.out.println("dao list size" + labTests.size());
			return labTests;
		} catch (HibernateException e) {
			// rollback();
			throw new Exception("Could not get user ");
		}
		
	}
	
}
