package com.me.HealTech;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.me.HealTech.dao.CitizenDAO;
import com.me.HealTech.dao.DoctorDAO;
import com.me.HealTech.dao.HibernateUtil;
import com.me.HealTech.dao.LabAssistantDAO;
import com.me.HealTech.dao.LabTestDataDAO;
import com.me.HealTech.dao.PatientDAO;
import com.me.HealTech.dao.PatientProfileDAO;
import com.me.HealTech.model.Citizen;
import com.me.HealTech.model.Doctor;
import com.me.HealTech.model.LabAssistant;
import com.me.HealTech.model.LabTestData;
import com.me.HealTech.model.Patient;
import com.me.HealTech.model.PatientProfile;
import com.me.HealTech.validator.DoctorModProfilesValidator;
import com.me.HealTech.validator.DoctorRegistrationValidator;
import com.me.HealTech.validator.DoctorSearchValidator;
import com.me.HealTech.validator.DoctorValidator;
import com.me.HealTech.validator.LabAssistantRegistrationValidator;
import com.me.HealTech.validator.LabAssistantValidator;
import com.me.HealTech.validator.PatientRegistrationValidator;
import com.me.HealTech.validator.PatientValidator;


/**
* Handles requests for the application home page.
*/
@Controller
public class HomeController {

                private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

                private Doctor gDoctor;
                private Patient gPatient;
                private LabAssistant gLabAssistant;


                @Autowired
                private DoctorModProfilesValidator doctorModProfilesValidator;


                @Autowired
                private DoctorValidator doctorValidator;

                @Autowired
                private DoctorSearchValidator doctorSearchValidator;

                @Autowired
                private DoctorRegistrationValidator doctorRegistrationValidator;

                @Autowired
                private PatientRegistrationValidator patientRegistrationValidator;

                @Autowired
                private LabAssistantValidator labAssistantValidator;
                
                @Autowired
                private LabAssistantRegistrationValidator labAssistantRegistrationValidator;

                @Autowired
                private PatientValidator patientValidator;


                @Autowired
                private PatientDAO patientDAO;

                @Autowired
                private DoctorDAO doctorDAO;

                @Autowired
                private CitizenDAO citizenDAO;

                @Autowired
                private PatientProfileDAO patientProfileDAO;

                @Autowired
                private LabTestDataDAO labTestDataDAO;

                @Autowired
                private LabAssistantDAO labAssistantDAO;


                @InitBinder("doctorMod")
                private void initDoctorModBinder(WebDataBinder binder) {
                                binder.setValidator(doctorModProfilesValidator);                           
                }
                @InitBinder("labAssistantReg")
                private void initLabAssistantRegBinder(WebDataBinder binder) {
                                binder.setValidator(labAssistantRegistrationValidator);                 
                }
                
                @InitBinder("doctor")
                private void initDoctorBinder(WebDataBinder binder) {
                                binder.setValidator(doctorValidator);                    
                }

                @InitBinder("doctorSearch")
                private void initDoctorSearchBinder(WebDataBinder binder) {
                                binder.setValidator(doctorSearchValidator);                       
                }

                @InitBinder("doctorReg")
                private void initDoctorRegistrationBinder(WebDataBinder binder) {
                                binder.setValidator(doctorRegistrationValidator);                            
                }

                @InitBinder("patientReg")
                private void initPatientRegistrationBinder(WebDataBinder binder) {
                                binder.setValidator(patientRegistrationValidator);                           
                }

                @InitBinder("patient")
                private void initPatientBinder(WebDataBinder binder) {
                                binder.setValidator(patientValidator);
                }

                @InitBinder("labAssistant")
                private void initLabAssistantBinder(WebDataBinder binder) {
                                binder.setValidator(labAssistantValidator);
                }

                @RequestMapping(value = "/", method = RequestMethod.GET)
                public String home(Locale locale, Model model) {
                                return "home";
                }

                @RequestMapping(value = "/", method = RequestMethod.POST)
                public String home(Locale locale, Model model, @RequestParam("userType") String userType) throws Exception
                {
                                /*Session session = HibernateUtil.getSessionFactory().openSession();
                                Transaction t = session.beginTransaction();

                                Patient patient = new Patient();
                                patient.setUsername("punita");
                                patient.setPassword("punita");
                                Doctor doctor = new Doctor();
                                LabTestData labTestData = new LabTestData();
                                labTestData.setPatient(patient);;
                                PatientProfile patientProfile = new PatientProfile();
                                patientProfile.setPatient(patient);
                                LabAssistant labAssistant = new LabAssistant();
                                Citizen citizen = new Citizen();
                                session.persist(patientProfile);
                                session.persist(labAssistant);
                                session.persist(doctor);
                                session.persist(patient);
                                session.persist(labTestData);
                                session.persist(citizen);


                                t.commit();
                                session.close();*/

                                Doctor doctor = new Doctor();
                                Patient patient = new Patient();
                                LabAssistant labAssistant = new LabAssistant();
                                String username=null;
                                String password = null;
                                System.out.println("***************************************************************");
                                System.out.println(userType);
                                model.addAttribute(doctor);
                                model.addAttribute(patient);
                                model.addAttribute(labAssistant);
                                //Customer customer = new Customer();
                                model.addAttribute("userType",userType);

                                if(userType.equals("Doctor"))
                                {
                                                return "doctorLogin";
                                }
                                else if(userType.equals("Patient"))
                                {
                                                return "patientLogin";
                                }
                                else if(userType.equals("Lab Assistant"))
                                {              
                                                System.out.println("*********************************/////////////////////////////");
                                                return "labAssistantLogin";
                                }

                                return null;

                }


                @RequestMapping(value = "logout")
                public String goTologout() {
                                return "home";
                }

                @RequestMapping(value = "doctorSettings")
                public String goTodoctorSettings(Model model, Locale locale, HttpServletRequest request) throws Exception
                {
                                model.addAttribute(new Doctor());
                                /*model.addAttribute("patient", new Patient());
                                model.addAttribute("patientProfile", new PatientProfile());*/
                                HttpSession session = request.getSession();
                                int doctorID =  (Integer) session.getAttribute("doctorID");
                                //System.out.println("username: " + username);
                                Doctor doctor  = doctorDAO.queryByDoctorID(doctorID);
                                model.addAttribute("SettingsDoctor",doctor);
                                
                                return "doctorSettings";
                }

                @RequestMapping(value = "taskList")
                public String goTotaskList() {
                                return "taskList";
                }


                @RequestMapping(value = "myPatients")
                public String goTomyPatients(Model model, HttpServletRequest request) throws Exception
                {
                                System.out.println("printing from go to my patients");
                                model.addAttribute("patient",new Patient());
                                model.addAttribute("doctor", new Doctor());
                                HttpSession session = request.getSession();
                                int doctorID = (Integer) (session.getAttribute("doctorID"));
                                System.out.println("doctorID: " + doctorID);
                                List<Patient> patientList = patientDAO.queryByDoctorID(doctorID);
                                System.out.println("patientlist: " + patientList);
                                model.addAttribute("myPatientList",patientList);
                                return "myPatients";
                }

                @RequestMapping(value = "doctorSearch")
                public String goToDoctorSearch(Locale locale, Model model) {
                                System.out.println("printing from patient search request mapping;");

                                Patient doctorSearch = new Patient();
                                model.addAttribute(doctorSearch);
                                return "doctorSearch";
                }
                @RequestMapping(value = "doctorRegistration")
                public String goToDoctorRegistration(Locale locale, Model model) {
                                Doctor doctor = new Doctor();
                                model.addAttribute(doctor);

                                return "doctorRegistration";
                }


                @RequestMapping(value = "patientRegistration")
                public String goToPatientRegistration(Locale locale, Model model) {

                                System.out.println("printing from patient registration request mapping;");

                                Patient patient = new Patient();
                                model.addAttribute(patient);
                                return "patientRegistration";
                }



                @RequestMapping(value = "labAssistantRegistration")
                public String goToLabAssistantRegistration(Locale locale, Model model) {
                                System.out.println("printing from labassistant registration request mapping;");

                                LabAssistant labAssistant = new LabAssistant();
                                System.out.println("//" +labAssistant);   
                                model.addAttribute(labAssistant);
                                                System.out.println(labAssistant);    
                                return "labAssistantRegistration";
                }
                @RequestMapping(value = "printProfile")
                public String goToPrintProfile(Locale locale, Model model, HttpServletRequest request, HttpServletResponse response
                                                /*@RequestParam("fname") String fname, 
                                                @RequestParam("lname") String lname, @RequestParam("country") String country, @RequestParam("street") String street,
                                                @RequestParam("apt") String apt, @RequestParam("city") String city, @RequestParam("zipcode") String zipcode,
                                                @RequestParam("homeNumber") String homeNumber, @RequestParam("cellNumber") String cellNumber, @RequestParam("allergies") String allergies,
                                                @RequestParam("medicinalAllergies") String medicinalAllergies, @RequestParam("disease") String disease, @RequestParam("patient") Patient patient*/)
                                                                                throws DocumentException, IOException  {
                                
                                /*model.addAttribute("patient",patient);*/
                                response.setContentType("application/pdf");
                       Enumeration street = request.getParameterNames();
                      
                       
        Document document  = new Document();
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        /*String fname = request.getParameter("street");*/
       /* String lname = (String) request.getAttribute("class");*/
     /*   while(street.hasMoreElements())
                                {
                                                String paramName = (String)street.nextElement();
                                                String paramValues[] = request.getParameterValues(paramName);                                                                                       
                                                document.add(new Paragraph(paramName));*/
                                                String temp,temp1= null;
        
        
        String paramValues[] = request.getParameterValues("street");
                                                for(int i=0; i< paramValues.length; i++)
                                                {
                                                                //out.println(paramValues[i]+ " ");
                                                                temp = paramValues[i];
                                                                //p.add(temp);
                                                                temp1 = "********************************************************************************************";
                                                                                                                document.add(new Paragraph(temp));
                                                                                                                
                                                }
                                                document.add(new Paragraph(temp1));
                                                
                                /*}*/
        
        
        
        /*PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        document.add(new Paragraph("Hello World" + fname));*/
        document.add(new Paragraph(new Date().toString()));
        document.close();
                                
                                return "printProfile";
                }
                @RequestMapping(value = "createPatientProfiles")
                public String goToCreatePatientProfiles(Model model, HttpServletRequest request)
                {
                                
                                //int id = Integer.parseInt(request.getParameter("hiddenpatientid"));
                                System.out.println("printing create patient profiles" + request.getParameter("hiddenpatientid"));
                                model.addAttribute("patientID", request.getParameter("hiddenpatientid"));
                                return "createPatientProfiles";
                }
                
                
                @RequestMapping(value = "viewPatientProfiles")
                public String goToViewPatientProfiles() {
                                return "viewPatientProfiles";
                }

                @RequestMapping(value = "doctorNavigation")
                public String goToNavigation() {
                                return "doctorNavigation";
                }

                @RequestMapping(value = "doctorLogin", method = RequestMethod.POST)
                public String doctorLogin(Model model,@Validated Doctor doctor, BindingResult result,  HttpServletRequest request)
                                                /*, @RequestParam("username") String username, @RequestParam("password") String password*/
                                                //) 
                                                                                throws Exception
                {
                                
                                /*String username = request.getParameter("username");
                                String password = request.getParameter("password");*/
                                String username = doctor.getUsername();
                                String password = doctor.getPassword();
                                
                                
                                if (result.hasErrors()) {

                                                return "doctorLogin";
                                }else{
                                
                                System.out.println("in the employee login code");
                                //Employee employee = new Employee();
                                model.addAttribute(doctor);
                                System.out.println("username: " + username);
                                System.out.println("password: " + password);

                                Doctor doc  = doctorDAO.queryUserByNameAndPassword(username, password);
                                System.out.println("username: " + username);
                                System.out.println("password: " + password);
                                System.out.println("employee: " + doc);
                                gDoctor = doc;
                                model.addAttribute("doctor", doc);
                                int doctorID = doc.getDoctorID();
                                System.out.println("doctorID: " + doctorID);
                                model.addAttribute("doctorID", doc.getDoctorID());
                                HttpSession session = request.getSession();
                                session.setAttribute("doctorID", doc.getDoctorID());
                                return "doctorHomePage";                         

                                }
                }
                
                @RequestMapping(value = "patientLogin", method = RequestMethod.POST)
                public String doctorLogin(Model model,@Validated Patient patient, BindingResult result,  HttpServletRequest request)
                                                /*, @RequestParam("username") String username, @RequestParam("password") String password*/
                                                //) 
                /*@RequestMapping(value = "patientLogin", method = RequestMethod.POST)
                public String patientLogin(Locale locale, Model model, HttpServletRequest request,
                                                @Validated Patient patient, 
                                                @RequestParam("username") String username, @RequestParam("password") String password,
                                                @ModelAttribute("patient") Patient patient) */
                                                                                throws Exception
                {
                	  if (result.hasErrors()) {

                          return "patientLogin";
          }else{
                                System.out.println("in the employee login code");
                                //Employee employee = new Employee();
                                model.addAttribute(new Patient());
                                String username = request.getParameter("username");
                                String password = request.getParameter("password");
                                
                                System.out.println("username: " + username);
                                System.out.println("password: " + password);
                                HttpSession session = request.getSession();
                                Patient pat  = patientDAO.queryUserByNameAndPassword(username, password);
                                System.out.println("username: " + username);
                                System.out.println("password: " + password);
                                System.out.println("patient: " + pat);
                                session.setAttribute("patientLoggedIn", username);
                                gPatient = pat;
                                model.addAttribute("patient", pat);
                                model.addAttribute("patientLoggedIn", username);
                                System.out.println("patientID: " + pat.getPatientID());
                                PatientProfile patientProfile = patientProfileDAO.getPatientProfile(pat.getPatientID());
                                System.out.println("patientProfile: " + patientProfile);
                                model.addAttribute("patientProfile",patientProfile);


                                List<LabTestData> labTests = labTestDataDAO.getLabTests(pat.getPatientID());
                                /*System.out.println("labtests: " + labTests + labTests.size() + labTests.get(0).getLabTestID());*/

                                model.addAttribute("labTests",labTests);

                                System.out.println(pat+"******************************");
                                /*model.addAttribute("patientByID",pat);
                                model.addAttribute("patientIDforref",patientID);*/
                                
                                
                                
                                return "patientHomePage";                        

          }
                }

                @RequestMapping(value = "patientRegistrationSuccess", method = RequestMethod.POST)
                public String patientRegistrationSuccess(Locale locale, Model model, @RequestParam("PatientID") String PatientID, 
                                                @RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("fname") String fname, 
                                                @RequestParam("lname") String lname, @RequestParam("country") String country, @RequestParam("street") String street,
                                                @RequestParam("apt") String apt, @RequestParam("city") String city, @RequestParam("zipcode") String zipcode,
                                                @RequestParam("homeNumber") String homeNumber, @RequestParam("cellNumber") String cellNumber, @RequestParam("allergies") String allergies,
                                                @RequestParam("medicinalAllergies") String medicinalAllergies, @RequestParam("disease") String disease, 
                                                @ModelAttribute("patient") Patient patient) {

                                System.out.println("printing from patient registration success");

                                //Patient patient = new Patient();
                                model.addAttribute("patient", new Patient());


                                String returnVal = "PatientRegistrationSuccessful";

                                /*if (result.hasErrors()) {
                                                return "patientRegistration";
                                } else {*/
                                try {
                                                /*patientDAO.insertPatient(patient.getPatientID(), patient.getUsername(), patient.getPassword(),patient.getCountry(),patient.getFname(),
                                                                                                patient.getLname(),patient.getStreet(),patient.getApt(), patient.getCity(),patient.getZipcode(),patient.getHomeNumber(),patient.getCellNumber());
                                                                return returnVal;*/
                                                patientDAO.insertPatient(Integer.parseInt(PatientID), username, password, country, fname, lname,  street, apt, city, zipcode, homeNumber, 
                                                                                cellNumber);

                                                /*int patientID, String username, String password, String country, String fname, String lname,
                                                                String street, String apt, String city, String zipcode, String homeNumber, String cellNumber*/

                                } catch (Exception e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                }
                                //}
                                return "PatientRegistrationSuccessful";

                }
                
                
                @RequestMapping(value = "labAssistantRegistrationSuccessful", method = RequestMethod.POST)
                public String labAssistantRegistrationSuccessful(Locale locale, Model model,
                                                @RequestParam("employeeID") String employeeID, 
                                                @RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("department") String department,
                                                @ModelAttribute("labAssistant") LabAssistant labAssistant) {

                                System.out.println("printing from labAssistant registration successful");

                                //Patient patient = new Patient();
                                
                                model.addAttribute("labAssistant", new LabAssistant());


                                /*String returnVal = "labAssistantRegistrationSuccessful";*/

                                /*if (result.hasErrors()) {
                                                return "patientRegistration";
                                } else {*/
                                try {
                                                
                                                labAssistantDAO.insertLabAssistant(Integer.parseInt(employeeID), department,username, password);
                                                } 
                                
                                catch (Exception e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                }
                                //}
                                return "labAssistantRegistrationSuccessful";

                }
                
                @RequestMapping(value = "labAssistantLogin", method = RequestMethod.POST)
                public String doctorLogin(Model model,@Validated LabAssistant labAssistant, BindingResult result,  HttpServletRequest request)
                
                                                                                throws Exception
                
                
                {
                	  if (result.hasErrors()) {

                          return "labAssistantLogin";
          }else{

        	  
        	  String username = request.getParameter("username");
        	  String password = request.getParameter("password");
                                System.out.println("in the employee login code");
                                //Employee employee = new Employee();
                                model.addAttribute(labAssistant);
                                System.out.println("username: " + username);
                                System.out.println("password: " + password);

                                LabAssistant doc  = labAssistantDAO.queryUserByNameAndPassword(username, password);
                                System.out.println("username: " + username);
                                System.out.println("password: " + password);
                                System.out.println("employee: " + doc);
                                gLabAssistant = doc;
                                model.addAttribute("labAssistant", doc);

                                return "labAssistantSearch";                       
          }

                }


                @RequestMapping(value = "doctorRegistrationSuccess", method = RequestMethod.POST)
                public String doctorRegistrationSuccess(Locale locale, Model model, @RequestParam("doctorID") String doctorID, 
                                                @RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("fname") String fname, 
                                                @RequestParam("lname") String lname, @RequestParam("country") String country, @RequestParam("street") String street,
                                                @RequestParam("apt") String apt, @RequestParam("city") String city, @RequestParam("zipcode") String zipcode,
                                                @RequestParam("homeNumber") String homeNumber, @RequestParam("cellNumber") String cellNumber, @RequestParam("license") String license,
                                                @RequestParam("specialty") String specialty, @ModelAttribute("doctor") Doctor doctor) {

                                System.out.println("printing from doctor registration success");

                                //Doctor doctor = new Doctor();
                                model.addAttribute("doctor", new Doctor());

                                /*String returnVal = "doctorRegistrationSuccess";*/

                                /*if (result.hasErrors()) {
                                                return "doctorRegistration";
                                } else {*/
                                try {
                                                /*doctorDAO.insertDoctor(doctor.getDoctorID(), doctor.getUsername(), doctor.getPassword(),doctor.getCountry(),doctor.getFname(),
                                                                                                doctor.getLname(),doctor.getStreet(),doctor.getApt(), doctor.getCity(),doctor.getZipcode(),doctor.getHomeNumber(),doctor.getCellNumber());
                                                                return returnVal;*/
                                                doctorDAO.insertDoctor(Integer.parseInt(doctorID), username, password, license, country, fname, lname,  
                                                                                street, apt, city, zipcode, homeNumber, cellNumber, specialty);



                                                /*int doctorID, String username, String password, String country, String fname, String lname,
                                                                String street, String apt, String city, String zipcode, String homeNumber, String cellNumber*/

                                } catch (Exception e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                }
                                //}
                                return "doctorRegistrationSuccess";

                }

                
                @RequestMapping(value="viewprofiles", method = RequestMethod.POST)
                public String viewPatientProfiles(Locale locale, Model model, HttpServletRequest request, @ModelAttribute("patient") Patient patient) throws Exception
                {
                                System.out.println("printing from view profiles");
                                model.addAttribute("patient", new Patient());
                                model.addAttribute("patientProfile", new PatientProfile());
                                String username = request.getParameter("myPatient");
                                System.out.println("username: " + username);
                                Patient pat  = patientDAO.queryOneUserByUsername(username);
                                System.out.println("pat: " + pat);
                                int patientID = pat.getPatientID();
                                HttpSession session = request.getSession();
                                session.setAttribute("patientIDSession", patientID);
                                System.out.println("patientID: " + patientID);
                                PatientProfile patientProfile = patientProfileDAO.getPatientProfile(patientID);
                                System.out.println("patientProfile: " + patientProfile);
                                model.addAttribute("patientProfile",patientProfile);


                                List<LabTestData> labTests = labTestDataDAO.getLabTests(patientID);
                                /*System.out.println("labtests: " + labTests + labTests.size() + labTests.get(0).getLabTestID());*/

                                model.addAttribute("labTests",labTests);

                                System.out.println(pat+"******************************");
                                model.addAttribute("patientByID",pat);
                                model.addAttribute("patientIDforref",patientID);
                                
                                return "viewprofiles";
                }

                @RequestMapping(value = "searchByID" , method = RequestMethod.POST)
                public String searchByID(Locale locale, Model model
                                                , @RequestParam("PatientID") String PatientID,@ModelAttribute("patient") Patient patient, HttpServletRequest request
                                                /*@Validated Doctor doctor*/
                                                ) throws NumberFormatException, Exception {

                                System.out.println("printing from searchByID");

                                Patient patient1 = new Patient();
                                //model.addAttribute("patient",new Patient());
                                model.addAttribute("patient",patient1);
                                //model.addAttribute("doctor", new Doctor());

                                Patient pat  = patientDAO.queryUserByID(Integer.parseInt(PatientID));
                                int patientID = pat.getPatientID();
                                HttpSession session = request.getSession();
                                session.setAttribute("patientIDSession", patientID);
                                System.out.println("patientID: " + patientID);
                                PatientProfile patientProfile = patientProfileDAO.getPatientProfile(patientID);
                                System.out.println("patientProfile: " + patientProfile);
                                model.addAttribute("patientProfile",patientProfile);

                                
                                List<LabTestData> labTests = labTestDataDAO.getLabTests(patientID);
                                System.out.println("labtests: " + labTests + labTests.size() + labTests.get(0).getLabTestID());

                                model.addAttribute("labTests",labTests);

                                System.out.println(pat+"******************************");
                                model.addAttribute("patientByID",pat);
                                model.addAttribute("patientIDforref",patientID);
                                return "viewprofiles";
                }

                @RequestMapping(value = "searchByName" , method = RequestMethod.POST)
                public String searchByName(Locale locale, Model model
                                                , @RequestParam("username") String username,@ModelAttribute("patient") Patient patient
                                                /*@Validated Doctor doctor*/
                                                ) throws NumberFormatException, Exception {

                                System.out.println("printing from searchByUsername method");

                                Patient patient1 = new Patient();
                                //model.addAttribute("patient",new Patient());
                                model.addAttribute("patient",patient1);
                                //model.addAttribute("doctor", new Doctor());

                                List<Patient> patientList  = patientDAO.queryUserByUsername(username);
                                System.out.println("patientlist size" + patientList.size());
                                model.addAttribute("patientListUsername",patientList);

                                return "viewProfilesByUsername";
                }


                @RequestMapping(value = "doctorModProfile" , method = RequestMethod.POST)
                public String doctorModProfile(Locale locale, Model model
                                                , @RequestParam("patientProfileID") String patientProfileID,@RequestParam("prescription") String prescription,
                                                @RequestParam("comments") String comments,@RequestParam("diagnosis") String diagnosis,
                                                @RequestParam("familyHistory") String familyHistory,@RequestParam("bloodGroup") String bloodGroup,
                                                @ModelAttribute("patientProfile") PatientProfile patientProfile
                                                /*@Validated Doctor doctor*/
                                                ) throws NumberFormatException, Exception
                {
                                System.out.println("printing from doctor mod profile");
                                //int patientID = patientProfileDAO.getPatientID(Integer.parseInt(patientProfileID));
                                model.addAttribute("patientProfile",new PatientProfile());
                                /*int patientProfileID, String prescription, String comments, String diagnosis,String familyHistory, String bloodGroup*/
                                patientProfileDAO.updatePatientProfile(Integer.parseInt(patientProfileID), prescription, comments, diagnosis, familyHistory, bloodGroup);
                                
                                
                                
                                return "home";
                }
                
                @RequestMapping(value = "doctorInsertNewProfile" , method = RequestMethod.POST)
                public String doctorInsertNewProfile(Locale locale, Model model, HttpServletRequest request
                                                /*, @RequestParam("patientProfileID") String patientProfileID
                                                ,@RequestParam("prescription") String prescription,
                                                @RequestParam("comments") String comments,@RequestParam("diagnosis") String diagnosis,
                                                @RequestParam("familyHistory") String familyHistory,@RequestParam("bloodGroup") String bloodGroup*/
                                                //,@ModelAttribute("patientProfile") PatientProfile patientProfile
                                                /*@Validated Doctor doctor*/
                                                ) throws NumberFormatException, Exception
                {
                                String prescription = request.getParameter("prescription");
                                String comments = request.getParameter("comments");
                                String diagnosis = request.getParameter("diagnosis");
                                String familyHistory = request.getParameter("familyHistory");
                                String bloodGroup = request.getParameter("bloodGroup");
                                int patientID = Integer.parseInt(request.getParameter("patientProfileID"));       
                                System.out.println("printing from doctor insert new profile");
                                System.out.println(prescription + comments + diagnosis+ familyHistory+ bloodGroup);
                                
                                model.addAttribute("patientProfile",new PatientProfile());
/*                           patientProfileDAO.insertNewPatientProfile(Integer.parseInt(patientProfileID), prescription, comments, diagnosis, familyHistory, bloodGroup);*/
                                patientProfileDAO.insertNewPatientProfile(patientID,prescription, comments, diagnosis, familyHistory, bloodGroup);
                                
                                
                                return "home";
                }

                
                
                
                
                
                
    @RequestMapping(value = "doctorSettings" , method = RequestMethod.POST)
    public String doctorSettings(Locale locale, Model model, HttpServletRequest request
                  /*, @RequestParam("patientProfileID") String patientProfileID
                  ,@RequestParam("prescription") String prescription,
                  @RequestParam("comments") String comments,@RequestParam("diagnosis") String diagnosis,
                  @RequestParam("familyHistory") String familyHistory,@RequestParam("bloodGroup") String bloodGroup*/
                  //,@ModelAttribute("patientProfile") PatientProfile patientProfile
                  /*@Validated Doctor doctor*/
                  ) throws NumberFormatException, Exception
    {
           model.addAttribute(new Doctor());
           String username = request.getParameter("username");
           String password = request.getParameter("password");
           String fname = request.getParameter("fname");
           String lname = request.getParameter("lname");
           String specialty = request.getParameter("specialty");
           String street = request.getParameter("street");
           String apt = request.getParameter("apt");
           String city = request.getParameter("city");
           String country = request.getParameter("country");
           String homeNumber = request.getParameter("homeNumber");
           String cellNumber = request.getParameter("cellNumber");
           String license = request.getParameter("license");
           doctorDAO.updateDoctor(username, password, fname, lname, specialty, license, street, apt, city, country, homeNumber, cellNumber);
           return "home";
    }





/*@RequestMapping(value = "doctorInsertNewProfile" , method = RequestMethod.POST)
    public String doctorInsertNewProfile(Locale locale, Model model, HttpServletRequest request
                  , @RequestParam("patientProfileID") String patientProfileID
                  ,@RequestParam("prescription") String prescription,
                  @RequestParam("comments") String comments,@RequestParam("diagnosis") String diagnosis,
                  @RequestParam("familyHistory") String familyHistory,@RequestParam("bloodGroup") String bloodGroup
                  //,@ModelAttribute("patientProfile") PatientProfile patientProfile
                  @Validated Doctor doctor
                  ) throws NumberFormatException, Exception
    {
           String prescription = request.getParameter("prescription");
           String comments = request.getParameter("comments");
           String diagnosis = request.getParameter("diagnosis");
           String familyHistory = request.getParameter("familyHistory");
           String bloodGroup = request.getParameter("bloodGroup");
           int patientID = Integer.parseInt(request.getParameter("patientProfileID")); 
           System.out.println("printing from doctor insert new profile");
           System.out.println(prescription + comments + diagnosis+ familyHistory+ bloodGroup);
           
           model.addAttribute("patientProfile",new PatientProfile());
            patientProfileDAO.insertNewPatientProfile(Integer.parseInt(patientProfileID), prescription, comments, diagnosis, familyHistory, bloodGroup);
           patientProfileDAO.insertNewPatientProfile(patientID,prescription, comments, diagnosis, familyHistory, bloodGroup);
           
           
           return "home";
    }*/




/*    @RequestMapping(value = "doctorModProfile" , method = RequestMethod.POST)
    public String doctorModProfile(Locale locale, Model model
                  , @RequestParam("patientProfileID") String patientProfileID,@RequestParam("prescription") String prescription,
                  @RequestParam("comments") String comments,@RequestParam("diagnosis") String diagnosis,
                  @RequestParam("familyHistory") String familyHistory,@RequestParam("bloodGroup") String bloodGroup,
                  @ModelAttribute("patientProfile") PatientProfile patientProfile
                  @Validated Doctor doctor
                  ) throws NumberFormatException, Exception
    {
           System.out.println("printing from doctor mod profile");
           //int patientID = patientProfileDAO.getPatientID(Integer.parseInt(patientProfileID));
           model.addAttribute("patientProfile",new PatientProfile());
           int patientProfileID, String prescription, String comments, String diagnosis,String familyHistory, String bloodGroup
           patientProfileDAO.updatePatientProfile(Integer.parseInt(patientProfileID), prescription, comments, diagnosis, familyHistory, bloodGroup);
           
           
           
           return "home";
    }*/

}
